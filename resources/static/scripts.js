function reloadSubtitle() {
  var subtitles = [
		'Отменить изменения? – Отменить / Отмена', 
		'Select purchase to purchase for $0.00 – PURCHASE / CANCEL', 
		'Это не текст, это ссылка. Не нажимайте на ссылку.',
		'Не обновляйте эту страницу! Не нажимайте НАЗАД',
		'Произошла ошибка — OK',
		'Пароль должен содержать заглавную букву и специальный символ',
		'Are you sure you want to exist? — YES / NO',
		'Открыть в приложении',
		'Warning: No pixels were selected'
  ];
  var subtitle = subtitles[Math.floor(Math.random() * subtitles.length)];
  var div = document.querySelector('.subtitle > span');
  div.innerHTML = subtitle;
}

var loader_status = 'IDLE';
var loader = document.querySelector(".loader");

function load_posts(){
    if(loader == null) return;
    loader_status = 'LOADING';
    loader.innerHTML = "loading ...";
    loader.classList.remove("loader-error");
    loader.removeEventListener("click", load_posts);
		var posts = document.querySelectorAll(".post"),
				last_post = posts[posts.length-1];
		last_post_id = last_post.getAttribute('data-id');
		//console.log("post-id" + last_post.getAttribute('data-id'));		

		fetch('/after/'+last_post_id).then(function(resp){
			  if(resp.ok){
				    resp.text().then(function (fragment){
					      if(fragment.length === 0){
						        loader_status = 'DONE';
                    loader.innerHTML = "the end";
                    loader.remove();//loader.parentNode.removeChild(loader);
					      }else{
						        var div = document.createElement('div');
						        div.innerHTML = fragment;
						        loader.parentNode.insertBefore(div,loader);
						        loader_status = 'IDLE';
                    loader.innerHTML = "";
					      }
			      });
			  }else{
            loader_status = "ERROR";// FIXME
            loader.innerHTML = "an error occoured";
            loader.classList.add("loader-error");
            loader.addEventListener("click", load_posts);
			  }
		});
}

function on_scroll(e){
	var cont = document.body,
			full_height = cont.scrollHeight,
			viewport_height = window.innerHeight,
			scrolled = window.pageYOffset || document.documentElement.scrollTop || cont.scrollTop,
			scrolled_bottom = scrolled + viewport_height;
			at_bottom = scrolled_bottom >= full_height - 200;
//	console.log(full_height +", "+ viewport_height +", "+ scrolled +", "+ at_bottom, loader_status);
	if(at_bottom && loader_status === 'IDLE'){
      load_posts();
	}

}

window.addEventListener("load", function() {
    reloadSubtitle();
    document.querySelector('.subtitle > span').onclick = reloadSubtitle;
    if (document.cookie.indexOf("grumpy_user=") >= 0) {
  	    console.log('checked');
        document.body.classList.remove("anonymous");
    }

    on_scroll();
    window.addEventListener('scroll', on_scroll);
});
