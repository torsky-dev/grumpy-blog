(ns grumpy.telegram
  (:require [clj-http.client :as http]
            [clojure.string :as str]
            [grumpy.core :as grumpy]))

(def token (grumpy/slurp "grumpy_data/TELEGRAM_TOKEN"))
(def channel (or (grumpy/slurp "grumpy_data/TELEGRAM_CHANNEL") "torskyChann"))
(def prefix (str "https://api.telegram.org/bot" token))

(defn post! [url params]
  (try
    (let [url'    (str "https://api.telegram.org/bot" token url)
          params' (assoc params :chat_id (str "@" channel))]
      (try
        (:body
         (http/post url'
                       { :form-params  params'
                        :content-type :json
                        :as           :json-string-keys
                        :coerce       :always }))
        (catch Exception e
          (println "Telegram request failed: " url' (pr-str params'))
          (throw e))))
    (catch Exception e
      (println "Telegram API error" (ex-data e))
      (throw e))))

;; https://grumpy.website/post/0TmO6niTo/M82N1mh.fit.jpeg
(defn post-picture! [post]
  (let [picture (:picture-original post)]
    (cond
      (grumpy/dev?)  post
      (nil? token)   post
      (nil? picture) post
      :else
      (let [photo-url  (str grumpy/hostname "/post/" (:id post) "/" (:url picture)) ;; FIXME grumpy/hostname
            resp       (post! "/sendPhoto" {:photo photo-url})]
        (update post :picture-original assoc
                :telegram/message_id  (get-in resp ["result" "message_id"])
                :telegram/photo       (get-in resp ["result" "photo"]))))))
;(slurp (str prefix "/sendMessage?chat_id=@torskyChann&text=hello!!?"))

(defn format-user [user]
  (if-some [telegram-user (:telegram/user (grumpy/author-by :user user))]
    (str "@" telegram-user)
    (str "*" user "*")))


(defn post-text! [post]
  (cond
    (nil? token)   post
    (str/blank? (:body post)) post
    :else
    (let [resp (post! "/sendMessage" { :text                     (str (format-user (:author post))  ": " (:body post))
                                       :parse_mode               "Markdown"
                                       :disable_web_page_preview "true" })]
      (assoc post :telegram/message_id (get-in resp ["result" "message_id"])))))


(defn update-text! [post]
  (let [id (:telegram/message_id post)]
    (cond
      (nil? token)   post
      (nil? id)      post
      :else
      (let [_ (post! "/editMessageText"
                     { :message_id               id
                       :text                     (str (format-user (:author post))  ": " (:body post))
                       :parse_mode               "Markdown"
                       :disable_web_page_preview "true" })]
        post))))

(comment

  (-> (clojure.edn/read-string (slurp (str "grumpy_data/posts/0TmO6niTo/post.edn")))
      (update-post!))


  (-> {:body "updated by bot!!!"
       :author "torsky"
       :telegram/message_id 62}
      (update-post!))


  (->
   (http/post (str prefix "/sendMessage")
                 {:form-params {:chat_id    "@torskyChann"
                                :text       "Multiline\n\nand *bold* "
                                :parse_mode "Markdown"
                                :disable_web_page_preview "true"}
                  :as :json-string-keys})
   println)

  (try
    (http/post (str prefix "/editMessageText")
               {:form-params {:chat_id    "@torskyChann"
                ;              :message_id 49
                              :text       "_updated_"
                              :parse_mode "Markdown"
                              :disable_web_page_preview "true"}
                :as :json-string-keys
                :coerce :always})
    (catch Exception e
      (ex-data e)))

  {:ok true, :result {:message_id 49, :chat {:id -1001328769181, :title torskyChann, :username torskyChann, :type channel}, :date 1590522803, :text Multiline and bold, :entities [{:offset 15, :length 4, :type bold}]}}


  (-> (http/post (str prefix "/sendPhoto")
                 {:form-params {:chat_id    "@torskyChann"
                                :photo      "https://grumpy.website/post/0TmO6niTo/M82N1mh.fit.jpeg"}
                  :as :json})
      :body
      )

  {:ok true, :result {:message_id 31, :chat {:id -1001328769181, :title torskyChann, :username torskyChann, :type channel}, :date 1590524015, :photo [{:file_id AgACAgQAAx0ETzNonQADH17NeG85CClGVDXaq7s63G9nbX6FAAKYqzEbpt1sUmNmaGnj87JIBYm3GwAEAQADAgADbQADxDAHAAEZBA, :file_unique_id AQADBYm3GwAExDAHAAE, :file_size 9380, :width 320, :height 127} {:file_id AgACAgQAAx0ETzNonQADH17NeG85CClGVDXaq7s63G9nbX6FAAKYqzEbpt1sUmNmaGnj87JIBYm3GwAEAQADAgADeAADwzAHAAEZBA, :file_unique_id AQADBYm3GwAEwzAHAAE, :file_size 41927, :width 800, :height 317} {:file_id AgACAgQAAx0ETzNonQADH17NeG85CClGVDXaq7s63G9nbX6FAAKYqzEbpt1sUmNmaGnj87JIBYm3GwAEAQADAgADeQADxTAHAAEZBA, :file_unique_id AQADBYm3GwAExTAHAAE, :file_size 60198, :width 1100, :height 436}]}}

  )
