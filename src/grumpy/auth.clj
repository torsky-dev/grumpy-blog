(ns grumpy.auth
  (:require
   [rum.core :as rum]
   [clojure.set :as set]
   [grumpy.core :as grumpy]
   [clojure.java.io :as io]
   [clojure.java.shell :as shell]
   [compojure.core :as compojure]
   [ring.middleware.session :as session]
   [ring.middleware.session.cookie :as session.cookie]
   [grumpy.core :as core]
   [clojure.set :as set]
   [clojure.string :as str])
  (:import [java.security SecureRandom]))

(def *tokens (atom {}))
(def token-ttl-ms (* 1000 60 15)) ;;15 mins
(def session-ttl-ms (* 1000 86400 14)) ;; 14 days


(defn random-bytes [size]
  (let [seed (byte-array size)]
    (.nextBytes (SecureRandom.) seed)
    seed))


(defn save-bytes! [file ^bytes bts]
  (with-open [os (io/output-stream (io/file file))]
    (.write os bts)))


(defn read-bytes [file len]
  (with-open [is (io/input-stream (io/file file))]
    (let [res (make-array Byte/TYPE len)]
      (.read is res 0 len)
      res)))


(when-not (.exists (io/file "grumpy_data/COOKIE_SECRET"))
  (save-bytes! "grumpy_data/COOKIE_SECRET" (random-bytes 16)))


(def cookie-secret (read-bytes "grumpy_data/COOKIE_SECRET" 16))


(defn send-mail! [{:keys [to subject body]}]
  (println "Email to:" to "\nSubject: " subject "\nBody: " body)
  #_(shell/sh
     "mail"
     "-s"
     subject
     to
     "-aFrom:Grumpy Admin <admin@grumpy.website>"
     :in body))


(defn gen-token []
  (str
   (grumpy/encode (rand-int Integer/MAX_VALUE) 5)
   (grumpy/encode (rand-int Integer/MAX_VALUE) 5)))


(defn get-token [email]
  (when-some [token   (get @*tokens email)]
    (let [created (:created token)]
      (when (<= (grumpy/age created) token-ttl-ms)
        (:value token)))))


(defn- expire-session [handler]
  (fn [req]
    (let [created (:created (:session req))]
      (if (and (some? created)
               (> (grumpy/age created) session-ttl-ms))
        (handler (dissoc req :session))
        (handler req)))))

(defn user [req]
  (get-in req [:session :user]))


(defn check-session [req]
  (when (nil? (user req))
    (println (str"logged user is " (user req)) )
    (grumpy/redirect "/forbidden" {:redirect-url (:uri req)})
    #_{:status 302
       :headers { "Location" (str "/forbidden?redirect=" (encode-uri-component (:uri req)))}}))


(defn force-user [handler]
  (fn [req]
    (if-some [u grumpy/forced-user]
      (some-> req
              (assoc-in [:session :user] u)
              (handler)
              (assoc :cookies { "grumpy_user" { :value u }}
                     :session { :user     u
                                :created  (grumpy/now) }))
      (handler req))))

(defn wrap-session [handler]
  (-> handler
      (expire-session)
      (force-user)
      (session/wrap-session
       {:store (session.cookie/cookie-store {:key cookie-secret})
        :cookie-name "grumpy_session"
        :cookie-attrs {:http-only true
                       :secure (not grumpy/dev?)
                       }})))

(rum/defc email-sent-page [message]
  (grumpy/page {:title "skoro tak ..."
                :styles ["authors.css"]}
             [:div.email_sent_message message]))


(rum/defc forbidden-page [redirect-url email]
  (grumpy/page {:title "Enter"
                :styles ["authors.css"]}
             [:form.forbidden {:action "/send-email"
                               :method "post"}
              [:.form_row
               [:input {:type "text" :name "email" :placeholder "email" :value "prokopov@gmail.com" :autofocus true}]]
              [:input {:type "hidden" :name "redirect-url" :value redirect-url}]
              [:.form_row
               [:button "send email"]]]
             #_[:a {:href (str "/authenticate?user=nikitonsky&token=ABC&redirect=" (encode-uri-component redirect))} "Login"]))


(compojure/defroutes routes
  (compojure/GET "/forbidden" [:as req]
                 (let [redirect-url (get (:params req) "redirect-url")
                       user         (get-in (:cookies req) ["grumpy_user" :value])
                       email        (:email (grumpy/author-by :user user))]
                   (grumpy/html-response
                    (forbidden-page redirect-url email))))

  (compojure/GET "/authenticate" [:as req] ;?user=...&token=...&redirect-url=...
                 (println (str "params:" (:params req)))
                 (let [email         (get (:params req) "email")
                       user          (:user (grumpy/author-by :email email))
                       token         (get (:params req) "token")
                       redirect-url  (get (:params req) "redirect-url")]
                   (if (= token (get-token email))
                     (do
                       (swap! *tokens dissoc email)
                       (assoc
                        (grumpy/redirect redirect-url)
                        :cookies { "grumpy_user" { :value user }}
                        :session { :user     user
                                   :created  (grumpy/now)}))
                     {:status 403
                      :body "403 bad token"})))

  (compojure/GET "/logout" [:as req]
                 (assoc
                  (grumpy/redirect "/")
                  :session nil))

  (compojure/POST "/send-email" [:as req]
                  (let [params     (:params req)
                        email      (get params "email")
                        user       (:user (grumpy/author-by :email email))]
                    (cond
                      (nil? (grumpy/author-by :email email))
                        (grumpy/redirect "/email-sent" {:message (str "you are not author, " email)})
                      (some? (get-token email))
                        (grumpy/redirect "/email-sent" {:message (str  "token still alive, check your email, " email)})
                      :else
                        (let [token      (gen-token)
                              redirect-url   (get params "redirect-url")
                              link       (grumpy/url (str (str/trim grumpy/hostname) "/authenticate")
                                                   {:email email
                                                    :token token
                                                    :redirect-url redirect-url})]
                          (swap! *tokens assoc email {:value token :created (grumpy/now)})
                          (send-mail! {:to email
                                       :subject (str "welocme to grumpy" (grumpy/format-date (grumpy/now)))
                                       :body (str "<html><div style='text-align: center;'><a href='" link "' style='display: inline-block;font-size:16px;
                                                padding:0.5em; 1.75em; background:#c3c;color:white;text-decoration:none;border-radius:4px'>
                                                login to site!</a></div></html>")})
                          (grumpy/redirect "/email-sent" {:message (str "check your email, " email "<br/> here is link: " link)} )))
                    ))

  (compojure/GET "/email-sent" [:as req]
                 (grumpy/html-response (email-sent-page (get-in req [:params "message"])))))
