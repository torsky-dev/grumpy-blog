(ns grumpy.server
  (:require
   [compojure.route]
   [rum.core :as rum]
   [clojure.stacktrace]
   [ring.util.response]
   [clojure.edn :as edn]
   [immutant.web :as web]
   [clojure.string :as str]
   [ring.middleware.params]

   [compojure.core :as compojure]

   [grumpy.db :as db]
   [grumpy.auth :as auth]
   [grumpy.core :as grumpy]
   [grumpy.authors :as authors])
  (:import
   [java.util Date])
  (:gen-class))


(def ^:const page-size 3)

(rum/defc post [post]
  [:.post
   {:data-id (:id post)}
   [:.post_side
    [:img.post_avatar
     {:src (if (some? (grumpy/author-by :user (:author post)))
             (str "/static/" (:author post) ".jpg")
             "/static/guest.jpg")}]]
   [:.post_content
    (when-some [pic      (-> post :picture)]
      (let [src      (str "/post/" (:id post) "/" (:url pic))
            href     (if-some [orig (:picture-original post)]
                       (str "/post/" (:id post) "/" (:url orig))
                       src)]
        (case (grumpy/content-type pic)
          :content.type/video
          [:video.post_video {:autoplay :true :loop true}
           [:source {:type (grumpy/mime-type (:url pic)) :src src}]]
          :content.type/image
          (if-some [[w h] (:dimensions pic)]
            (let [[w' h'] (grumpy/fit w h 550 500)]
              [:div { :style { :max-width w' }}
               [:a.post_img.post_img-fix
                { :href href
                 :target "_blank"
                 :style { :padding-bottom (-> (/ h w) (* 100) (double) (str "%")) }}
                [:img { :src src }]]])
            [:a.post_img.post_img-flex { :href href, :target "_blank" }
             [:img { :src src }]]))))
    [:.post_body
     {:dangerouslySetInnerHTML
      {:__html (grumpy/format-text
                (str "<span class=\"post_author\">" (:author post) ": </span>" (:body post))) }}]
    [:p.post_meta
     (grumpy/format-date (:created post))
     " // " [:a {:href (str "/post/" (:id post))} "Hyperlink"]
     [:a.post_meta_edit {:href (str "/post/" (:id post) "/edit")} "Edit"]]]])

(rum/defc index-page [post-ids]
  (grumpy/page {:page :index :scripts ["loader.js"]}
               (for [post-id post-ids]
                 (post (grumpy/get-post post-id)))))

(rum/defc posts-fragment [post-ids]
  (for [post-id post-ids]
    (post (grumpy/get-post post-id))))


(rum/defc post-page [post-id]
  (grumpy/page {:page :post}
    (post (grumpy/get-post post-id))))


(defn read-session [handler]
  (fn [req]
    (let [session (some-> (get-in req [:cookies "session" :value])
                          (edn/read-string))
          req     (if (some? session)
                    (assoc req :user (:user session))
                    req)]
      (handler req))))


(defn feed [post-ids]
  (let [posts (map grumpy/get-post post-ids)
        updated (->> posts
                     (map :updated)
                     (map #(.getTime ^Date %))
                     (reduce max)
                     (java.util.Date.))]
    (str "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
         "<feed xmlns=\"http://www.w3.org/2005/Atom\" xml:lang=\"en-US\">\n"
         "  <title>grumpy website</title>\n"
         "  <link type=\"application/atom+xml\" href=\"" grumpy/hostname "/feed.xml\" rel=\"self\"/>\n"
         "  <link rel=\"alternate\" type=\"text/html\" href=\"" grumpy/hostname "\"/>\n"
         "  <id>"grumpy/hostname"</id>\n"
         "  <updated>" (grumpy/format-iso-inst updated)  "</updated>\n"
         (str/join ""
                   (for [author grumpy/authors]
                     (str "  <author><name>"(:name author)"</name><email>"(:email author)"</email></author>\n")))
         (str/join ""
                   (for [post posts
                         :let [author (grumpy/author-by :user (:author posts))]]
                     (str "\n  <entry>\n"
                          "    <title>Cleaning up form UI</title>\n"
                          "    <link rel=\"alternate\" type=\"text/html\" href=\"" grumpy/hostname "/posts/" (:id post) "\"/>\n"
                          "    <id>" grumpy/hostname "/posts/" (:id post)"</id>\n"
                          "    <published>"(grumpy/format-iso-inst (:created post))"</published>\n"
                          "    <updated>"(grumpy/format-iso-inst (:updated post))"</updated>\n"
                          "    <content type=\"html\"><!CDATA[\n"
                          (str/join ""
                                    (when-some [pic (-> post :picture :url)]
                                      (str "      <p><img src="\" grumpy/hostname "/post/" (:id post) "/" pic "\" ></p> \n")))
                          (str/join ""
                                    (for [[paragraph idx] (grumpy/zip (str/split (:body post) #"(\r?\n)+") (range))]
                                      (str "      <p>"
                                       (when (== 0 idx)
                                         (str "<strong>" (:author post) "</strong>"))
                                       paragraph
                                       "</p>")))
                          "]]>\n    </content>\n"
                          (str "    <author><name>" (:name author) "</name><email>" (:email author) "</email></author>\n")
                          "  </entry>\n"
                          )))
         "</feed>")))


(defn sitemap [post-ids]
  (str
   "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
   "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n"
   "<url><loc>" grumpy/hostname"/</loc></url>\n"
   (str/join "\n"
             (for [id post-ids]
               (str "<url><loc>" grumpy/hostname "/post/" id "</loc></url>")))
   "</urlset>"))

(compojure/defroutes routes

  (compojure/GET "/post/:post-id/:img" [post-id img]
                 (ring.util.response/file-response (str "grumpy_data/posts/" post-id "/" img)))

  (compojure/GET "/post/:post-id" [post-id]
                 (grumpy/html-response (post-page post-id)))

  (compojure/GET "/after/:post-id" [post-id]
                 (when grumpy/dev? (Thread/sleep 2000))
                 (if (and grumpy/dev? (< (rand) 0.5)) ;; ako by sa to spravalo keby nastala chyba
                   { :status 500 }
                   (let [post-ids (->> (grumpy/post-ids)
                                       (drop-while #(not= % post-id))
                                       (drop 1)
                                       (take page-size))
                         ]
                     {:status 200
                      :headers {"Content-Type" "text/html; charset=utf-8"}
                      :body (rum/render-static-markup (posts-fragment post-ids))})))

  (compojure/GET "/feed.xml" []
                 {:status 200
                  :headers {"Content-Type" "application/atom+xml; charset=utf-8"}
                  :body (feed (take 10 (grumpy/post-ids)))})

  (compojure/GET "/sitemap.xml" []
                 {:status 200
                  :headers {"Content-Type" "application/xml; charset=utf-8"}
                  :body (sitemap (grumpy/post-ids))})

  (compojure/GET "/robots.txt" []
                 {:status 200
                  :headers {"Content-Type" "text/plain"}
                  :body (grumpy/resource "robots.txt")})


  (auth/wrap-session
   (compojure/routes
    auth/routes
    authors/routes))

  ;; kvoli forced-user, pretoze nanutime prihlasenie ak sme na dev
  (cond->
   (compojure/GET "/" []
                  (let [post-ids   (grumpy/post-ids)
                        first-ids  (take (+ page-size (rem (count post-ids) page-size))  post-ids)]
                    (grumpy/html-response (index-page first-ids))))
    grumpy/dev? (auth/wrap-session))

  )


(defn with-headers [handler headers]
  (fn [request]
    (some-> (handler request)
      (update :headers merge headers))))


(defn print-errors [handler]
  (fn [req]
    (try
      (handler req)
      (catch Exception e
        (.printStackTrace e)
        { :status 500
          :headers { "Content-type" "text/plain; charset=utf-8" }
          :body (with-out-str
                  (clojure.stacktrace/print-stack-trace (clojure.stacktrace/root-cause e))) }))))

 
(def app
  (compojure/routes
   (->
    routes
    ;(auth/wrap-session) ;; toto sme dali uz do routes
    (ring.middleware.params/wrap-params)
    (with-headers { ;"Content-Type"  "text/html; charset=utf-8"
                   "Cache-Control" "no-cache"
                   "Expires"       "-1" })
    (print-errors))
   (->
    (compojure.route/resources "/static" {:root "static"})
    (with-headers (if grumpy/dev? ;; static resources does not have to be reloaded always in prod
                    { "Cache-Control" "no-cache"
                     "Expires"       "-1" }
                    { "Cache-Control" "max-age=315360000" })))
   (fn [req]
     { :status 404
       :body "404 Not found " })))


(defn -main [& args]
  (let [args-map (apply array-map args)
        port-str (or (get args-map "-p")
                     (get args-map "--port")
                     "8080")]
    (println "Starting web server on port" port-str)
    (web/run #'app { :port (Integer/parseInt port-str) })))





(comment
  (def server (-main "--port" "8080"))
  (web/stop server)
  (reset! auth/*tokens {})
  (figwheel-sidecar.repl-api/cljs-repl))
